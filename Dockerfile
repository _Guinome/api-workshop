FROM node:8
RUN mkdir /api-workshop
RUN npm install -g nodemon
RUN npm install -g sequelize-cli
COPY ./ /api-workshop
WORKDIR /api-workshop
RUN npm i
EXPOSE 80
CMD ["npm", "start"]