-- DROP TABLE IF EXISTS breeds;
-- CREATE TABLE breeds(
--     "breed_id" SERIAL PRIMARY KEY,
--     "breed_name" VARCHAR(255) NOT NULL,
--     "created_at" DATE,
--     "updated_at" DATE
-- );

INSERT INTO breeds ("breed_name","created_at","updated_at") VALUES ('Labrador',current_timestamp,current_timestamp);
INSERT INTO breeds ("breed_name","created_at","updated_at") VALUES ('Pug',current_timestamp,current_timestamp);
INSERT INTO breeds ("breed_name","created_at","updated_at") VALUES ('Golden Retriever',current_timestamp,current_timestamp);
INSERT INTO breeds ("breed_name","created_at","updated_at") VALUES ('Yorkshire',current_timestamp,current_timestamp);

-- DROP TABLE IF EXISTS dogs;
-- CREATE TABLE dogs(
--     "dog_id" SERIAL PRIMARY KEY,
--     "breed_id" INT NOT NULL,
--     "photo_url" VARCHAR(255),
--     "dog_name" VARCHAR(255),
--     "dog_description" TEXT,
--     "created_at" DATE,
--     "updated_at" DATE
-- );

INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/eE29vX4.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/xX2AeDR.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/hBFRUuW.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/WDWK4nF.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/zxtD5Zw.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/MrkAGKR.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/o3Nyw4R.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/SzP5370.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/oHaP6I3.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/kSU7Zca.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/SAJJ1oH.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/BYvRbs8.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/VzFTsGg.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/qigJZWa.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (1, 'http://i.imgur.com/gskym.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/ozJD7SC.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/E5vBM5Z.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/miiP4NI.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/GCE8dj5.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/NGG3Yir.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/q53cfRy.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/Flic3TB.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/zjgtrf9.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/Mda3xXr.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/IOh5mgB.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/Fl2ivvG.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/iLzWvSY.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/RQKBN3F.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/8c3RpI0.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/7ysJzS4.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/uccGfLn.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/HrscSnK.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/tUZhJYN.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/xAGJ0Ry.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/YxSvzWm.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/Y32LWO6.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/umJXx6S.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/R8Ju76x.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/xTg9j70.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/zUnR5lj.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/RUjOi8t.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/Dd1K1uR.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/Q5uksG8.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/7YxiavD.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/8B3HOrS.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/9YfjSxU.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/NSCW1Rz.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/ZwM4KY4.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/uy9pY3Y.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/ZFk0cgV.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/6Fz5JOg.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/mNQbxWo.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/C1MFoB0.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (2, 'http://i.imgur.com/Rdbawjx.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/wR38uBx.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/CHRlo0W.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/tXnch1O.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/VCgfaB2.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/Ror46i4.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/Ful5PwH.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/C4rrJdn.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/Ao9bR9A.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/R14AU0I.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/gpH8wIV.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/luPxoig.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/MiIDyfD.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/4qmmRFj.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/ZSzHQ2q.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/PoA6rLg.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/NYwynk3.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/ni8vK0U.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/NinMpda.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/4L8rMko.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/HoISzqN.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/FPISssi.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/SzP5370.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/oHaP6I3.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/wlVWJpY.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/rV4COi2.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/6WZFhJX.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/gFLzkPt.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/ULSe4AI.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/yDEa2a5.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/BYvRbs8.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/VzFTsGg.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/BnhbY54.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (3, 'http://i.imgur.com/ToYsMEC.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (4, 'http://i.imgur.com/oSieVUO.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (4, 'http://i.imgur.com/qtXIL.png', current_timestamp, current_timestamp);
INSERT INTO dogs ("breed_id", "photo_url", "created_at", "updated_at") VALUES (4, 'http://i.imgur.com/qWLKy8a.jpg', current_timestamp, current_timestamp);
