const breed = require('./breed');
const dog = require('./dog');

module.exports = {
  breed,
  dog,
};
