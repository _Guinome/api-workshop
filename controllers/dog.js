const Breed = require('../models').breed;
const Dog = require('../models').dog;

module.exports = {
  list(req, res) {
    return Dog
      .findAll({
        include: [{
          model: Breed,
          as: 'breed',
        }],
        order: [
          ['created_at', 'DESC'],
          [{ model: Breed, as: 'breed' }, 'created_at', 'DESC'],
        ],
      })
      .then(dogs => res.status(200).send(dogs))
      .catch((error) => { res.status(400).send(error); });
  },

  getById(req, res) {
    return Dog
      .findById(req.params.dog_id, {
        include: [{
          model: Breed,
          as: 'breed',
        }],
      })
      .then((dog) => {
        if (!dog) {
          return res.status(404).send({
            message: 'dog Not Found',
          });
        }
        return res.status(200).send(dog);
      })
      .catch(error => res.status(400).send(error));
  },

  add(req, res) {
    return Dog
      .create({
        breed_id: req.body.breed_id,
        dog_name: req.body.dog_name,
        dog_description: req.body.dog_description,
        photo_url: req.body.photo_url,
      })
      .then(dog => res.status(201).send(dog))
      .catch(error => res.status(400).send(error));
  },

  update(req, res) {
    return Dog
      .findById(req.params.dog_id, {
        include: [{
          model: Breed,
          as: 'breed',
        }],
      })
      .then((dog) => {
        if (!dog) {
          return res.status(404).send({
            message: 'dog Not Found',
          });
        }
        return dog
          .update({
            breed_id: req.body.breed_id || dog.breed_id,
            dogName: req.body.dogName || dog.dogName,
            dog_description: req.body.dog_description || dog.dog_description,
            photo_url: req.body.photo_url || dog.photo_url,
          })
          .then(() => res.status(200).send(dog))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },

  delete(req, res) {
    return Dog
      .findById(req.params.dog_id)
      .then((dog) => {
        if (!dog) {
          return res.status(400).send({
            message: 'dog Not Found',
          });
        }
        return dog
          .destroy()
          .then(() => res.status(204).send())
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },
};
