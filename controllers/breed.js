const Breed = require('../models').breed;
const Dog = require('../models').dog;

module.exports = {
  list(req, res) {
    return Breed
      .findAll({
        include: [{
          model: Dog,
          as: 'dogs',
        }],
        order: [
          ['created_at', 'DESC'],
          [{ model: Dog, as: 'dogs' }, 'created_at', 'DESC'],
        ],
      })
      .then(breeds => res.status(200).send(breeds))
      .catch((error) => { res.status(400).send(error); });
  },

  getById(req, res) {
    return Breed
      .findById(req.params.breed_id, {
        include: [{
          model: Dog,
          as: 'dogs',
        }],
      })
      .then((breed) => {
        if (!breed) {
          return res.status(404).send({
            message: 'breed Not Found',
          });
        }
        return res.status(200).send(breed);
      })
      .catch(error => res.status(400).send(error));
  },

  add(req, res) {
    return Breed
      .create({
        breed_name: req.body.name,
      })
      .then(breed => res.status(201).send(breed))
      .catch(error => res.status(400).send(error));
  },

  update(req, res) {
    return Breed
      .findById(req.params.breed_id, {
        include: [{
          model: Dog,
          as: 'dogs',
        }],
      })
      .then((breed) => {
        if (!breed) {
          return res.status(404).send({
            message: 'breed Not Found',
          });
        }
        return breed
          .update({
            breed_name: req.body.name || breed.breed_name,
          })
          .then(() => res.status(200).send(breed))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },

  delete(req, res) {
    return Breed
      .findById(req.params.breed_id)
      .then((breed) => {
        if (!breed) {
          return res.status(400).send({
            message: 'breed Not Found',
          });
        }
        return breed
          .destroy()
          .then(() => res.status(204).send())
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },
};
