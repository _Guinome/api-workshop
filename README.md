cf https://lab.hellocomet.co/snippets/49

 - Get code `git clone`
 - Build & up containers `docker-compose build` then `docker-compose up -d`
 - Optional `docker-compose logs -f`
 - Populate db `docker exec -d api-workshop_db_1 psql -U postgres -d comet_dogs -a -f comet_dogs.sql`


# Routes
breeds & dogs CRUD

## Breeds
`GET    /breeds`
`GET    /breeds/:id`
`POST   /breeds/:id`
`PUT    /breeds/:id`
`DELETE /breeds/:id`
Waited input for `POST` & `PUT`
```
{
    "name": "my awesome breed name"
}
```

## Dogs
`GET    /dogs`
`GET    /dogs/:id`
`POST   /dogs/:id`
`PUT    /dogs/:id`
`DELETE /dogs/:id`
Waited input for `POST` & `PUT`
```
{
    "breed_id": 0,
    "dog_name": "my dog name",
    "dog_description": "my dog description",
    "photo_url": "dog's photo url"
}

```