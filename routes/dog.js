
const express = require('express');

const router = express.Router();
const dogController = require('../controllers').dog;

/* GET dogs listing. */
router.get('/', dogController.list);

/* GET dog */
router.get('/:dog_id', dogController.getById);

/* POST dog */
router.post('/', dogController.add);

/* PUT dog */
router.put('/:dog_id', dogController.update);

/* DELETE dog */
router.delete('/:dog_id', dogController.delete);

module.exports = router;
