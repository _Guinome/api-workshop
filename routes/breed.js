const express = require('express');

const router = express.Router();
const breedController = require('../controllers').breed;

/* GET breeds listing. */
router.get('/', breedController.list);

/* GET breed */
router.get('/:breed_id', breedController.getById);

/* POST breed */
router.post('/', breedController.add);

/* PUT breed */
router.put('/:breed_id', breedController.update);

/* DELETE breed */
router.delete('/:breed_id', breedController.delete);

module.exports = router;
