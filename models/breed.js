
module.exports = (sequelize, DataTypes) => {
  const breed = sequelize.define('breed', {
    breed_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    breed_name: DataTypes.STRING,
  }, {
    underscored: true,
  });
  breed.associate = (models) => {
    // associations can be defined here
    breed.hasMany(models.dog, {
      foreignKey: 'breed_id',
      as: 'dogs',
    });
  };
  return breed;
};
